(function($) {
    'use strict';

    $(function() {
        var w = window.innerWidth;
        var h = window.innerHeight;


        // Article Items
        var $article    =   $('.article-item');
        $article.each(function() {
            var $this   =   $(this),
                $title   =    $this.find('.article-item-title'),
                $info   =   $this.find('.article-item-productinfo');
                
                $title.append($info);
        });
        

        // Articles appealing and desappealing on scroll
        $(document).on("scroll", function() {
            var pageBottom   =   $(document).scrollTop() + h,
                article    =   $('.article-item');

            for (var i = 0; i < article.length; i++) {
                var art = article[i];
                
                if ($(art).position().top < pageBottom) {
                        $(art).addClass("visible")
                } else {
                        $(art).removeClass("visible")
                }
            }
        });

        
    });

})(jQuery);
($)